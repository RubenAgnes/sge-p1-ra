# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a repository made by Rubén Agnès, student of the school Escola Pia Balmes, for the 10th module of the grade, "Sistemes de gestió empresarial". In this practice, we are creating a README file in a BitBucket repository and working in different branches of the repository using Source Tree.
* Version 1
* Escola Pia Balmes: http://balmes.escolapia.cat/

![git4.jpg](https://bitbucket.org/repo/9r6GB9/images/873307642-git4.jpg)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Rubén Agnès
* Escola Pia Balmes